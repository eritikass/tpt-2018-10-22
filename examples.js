const sum = require('./sum');
const time = require('./time');
const contact = require('./contact');

// sum two numbers
console.log('sum(1, 3)', sum(1, 3));

try {
  sum('a', 'b');
} catch (e) {
  console.log('sum usage error: ', e.message);
}

// get current time
time().then((currentTime) => {
  console.log('time', currentTime);
});

// get few contacts
console.log('contact:5', contact(5));
console.log('contact:78', contact(78));
